<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CommentRequest;
use App\Photo;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param CommentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        $comment = new Comment();
        $comment->body = $request->input('body');
        $comment->photo_id = $request->input('photo_id');
        $comment->user_id = $request->user()->id;
        $comment->save();

        return redirect()->route('photo', compact('comment'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param CommentRequest $request
     * @param int $comment_id
     * @param Photo $photo
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, int $comment_id,  Photo $photo)
    {
        $comment = Comment::findOrFail($comment_id);
        $comment->update($request->all());

        return view('photo.show', ['photo' => $photo]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Photo $photo)
    {
        $comment = Comment::findOrFail($id);
        $comment->delete();

        return view('photo.show', ['photo' => $photo]);
    }
}
