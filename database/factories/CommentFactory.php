<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'body' => $faker->text($maxNbChars = 300),
        'appraisal' => rand(1, 5),
        'user_id' => rand(1, 5),
        'photo_id' => rand(1, 10)
    ];
});
