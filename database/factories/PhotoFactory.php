<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Photo;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Faker\Generator as Faker;


/**
 * @param int $picture_number
 * @return string
 */
function photo_path(int $picture_number):string
{
    $path = storage_path() ."/seed_pictures/" . "$picture_number" . ".jpg";
    $picture_name = md5($path). ".jpg";
    $resize = Image::make($path)->fit(300)->encode('jpg');
    Storage::disk('public')->put('images/'.$picture_name, $resize->__toString());
    return 'images/'. $picture_name;
}

$factory->define(Photo::class, function (Faker $faker) {
    return [
        'images' => photo_path(rand(1,10)),
        'title' => $faker->title,
        'user_id' => rand(1,5),
    ];
});
