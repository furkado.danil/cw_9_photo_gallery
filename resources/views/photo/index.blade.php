@extends('layouts.app')

@section('content')
    <div class="row">
        @foreach($photos as $photo)
            <div class="card ml-5 mt-5" style="width: 18rem;">
                <img src="{{asset( '/storage/'. $photo->images)}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">{{$photo->title}}</h5>
                    <hr/>
                    <a href="{{route('user.show', ['user' => $photo->user->id])}}"><h4>{{$photo->user->name}}</h4></a>
                    <a href="{{route('photo.show', ['photo' => $photo])}}" class="btn btn-primary">@lang('Show')</a>
                </div>
            </div>
        @endforeach

    </div>

@endsection