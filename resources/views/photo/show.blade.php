@extends('layouts.app')

@section('content')
    <div class="row mt-4 mb-4">
        <a class="btn btn-primary" href="{{route('photo.index')}}" role="button">@lang('Back')</a>
    </div>
    <div>
        <img src="{{asset( '/storage/'. $photo->images)}}" class="img-fluid img-thumbnail" alt="Адаптивные изображения">
        <h5>@lang('Average score:')</h5>
    </div>

    @if(Auth::check())
    <div class="mt-5">
        <h3>@lang('Add comment')</h3>
        <form action="{{route('comments.store')}}" method="post">
            @csrf
            <div class="form-group row">
                <label for="text" class="col-sm-2 col-form-label">@lang('Text')</label>
                <div class="col-sm-10">
                    <textarea class="form-control" id="text" name="body" rows="3"></textarea>
                </div>
                <label for="text" class="col-sm-2 col-form-label mt-3">@lang('Score')</label>
                <div class="col-sm-10">
                    <select name="appraisal" class="form-control-sm mt-4">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">@lang('Add')</button>
            </div>
        </form>
    </div>
    @endif
<div class="mt-5">
    <h3>@lang('Comments')</h3>
    @foreach($photo->comments as $comment)
        <h4>@lang('Name:') {{$comment->user->name}}, @lang('score') {{$comment->appraisal}}</h4>
        <p>{{$comment->body}}</p>
        <hr>
    @endforeach
</div>
@endsection