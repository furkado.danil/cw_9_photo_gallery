@extends('layouts.app')

@section('content')
    <div class="mt-5"><h1>{{$user->name}}</h1></div>
    <div class="row">
        <h3>@lang('My photos:')</h3> <a class="btn btn-outline-success ml-5" href="">@lang('Create new photo')</a>
    </div>

    <div class="mt-4">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">@lang('Photo')</th>
                <th scope="col">@lang('Title')</th>
                <th scope="col">@lang('Action')</th>
            </tr>
            </thead>
            @if(isset($user->photos))
            @foreach($user->photos as $photo)
                <tbody>
                <tr>
                    <th scope="row"><img src="{{asset( '/storage/'. $photo->images)}}"  width="100" height="100" alt="@lang('Photo')"></th>
                    <td>{{$photo->title}}</td>
                    <td>
                        {{--@can('delete', $photo)--}}
                        <form action="{{ route('user.destroy', ['user' => $photo]) }}" method="post">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger mb-2">@lang('Delete')</button>
                        </form>
                        {{--@endcan--}}
                    </td>
                </tr>
                </tbody>
            @endforeach
                @endif
        </table>
    </div>


@endsection