<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>'language'],function ()
{
    Route::get('/', 'PhotoController@index')->name('home');

    Route::resource('photo', 'PhotoController')->only('index', 'show');

    Route::resource('comments', 'CommentController')->only('store', 'update', 'destroy');

    Route::resource('user', 'UserController')->only('store', 'show', 'destroy');


    Auth::routes();
});

Route::get('language/{locale}', 'LanguageSwitcherController@switcher')
    ->name('language.switcher')
    ->where('locale', 'en|ru');

